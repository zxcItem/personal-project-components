<?php

namespace Xiaochao\ItemLibrary;
use FFMpeg;

/**
 * 视频处理
 */
class VideoHandle
{

    /**
     * 视频生成视频封面
     * @param string $video 视频地址
     * @param string $image_path 封面图地址
     * @param int $width 封面图宽度
     * @param int $height 封面图高度
     * @param int $size 视频第几帧作为封面
     * @return bool
     */
    public static function videoCover(string $video,string $image_path,int $width, int $height,int $size = 1):bool
    {
        if (!empty($_SERVER['USER'])) {
            $command = "ffmpeg -i $video -y -f image2 -s $width*$height -frames $size $image_path";
            @shell_exec($command);
        }else{
            $ffmpeg = FFMpeg\FFMpeg::create(array(
                'ffmpeg.binaries'  => 'C:\ffmpeg\bin\ffmpeg.exe',
                'ffprobe.binaries' => 'C:\ffmpeg\bin\ffprobe.exe',
            ));
            $video = $ffmpeg->open($video);
            $video->filters()->resize(new FFMpeg\Coordinate\Dimension($width, $height))->synchronize();
            $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds($size))->save($image_path);
        }
        if (file_exists($image_path)) {
            return true;
        }
        return false;
    }
}